
# <a name="title"></a>Curriculum-Vitae

This is my Curriculum Vitae. I will try to keep it up-to-date as much as possible.  
It is from [**Awesome CV** <img alt="AwesomeCV" src="https://github.com/posquit0/Awesome-CV/raw/master/icon.png" width="40px" height="40px" />](https://github.com/posquit0/Awesome-CV), a open LaTeX template.  


## <a name="preview"></a>Download my CV

At this time I do not provide an easy way to download my CV, but it is on my todo list !

## <a name="preview"></a>Compile my CV

### Requirements

A full TeX distribution is assumed.  [Various distributions for different operating systems (Windows, Mac, \*nix) are available](http://tex.stackexchange.com/q/55437) but TeX Live is recommended.
You can [install TeX from upstream](http://tex.stackexchange.com/q/1092) (recommended; most up-to-date) or use `sudo apt-get install texlive-full` if you really want that.  (It's generally a few years behind.)

### Usage

At a command prompt, run

```bash
$ make
```

This should result in the creation of ``{your-cv}.pdf``  
Or alternately run

```bash
$ xelatex {your-cv}.tex
```
This should result in the creation of ``{your-cv}.pdf``  


## <a name="credit"></a>Credit

[**LaTeX**](http://www.latex-project.org) is a fantastic typesetting program that a lot of people use these days, especially the math and computer science people in academia.

[**LaTeX FontAwesome**](https://github.com/furl/latex-fontawesome) is bindings for FontAwesome icons to be used in XeLaTeX.

[**Roboto**](https://github.com/google/roboto) is the default font on Android and ChromeOS, and the recommended font for Google’s visual language, Material Design.

[**Source Sans Pro**](https://github.com/adobe-fonts/source-sans-pro) is a set of OpenType fonts that have been designed to work well in user interface (UI) environments.

[**Awesome CV**](https://github.com/posquit0/Awesome-CV) is LaTeX template for a *CV(Curriculum Vitae)*, *Résumé* or *Cover Letter* inspired by [Fancy CV](https://www.sharelatex.com/templates/cv-or-resume/fancy-cv). It is easy to customize your own template, especially since it is really written by a clean, semantic markup.


## <a name="contact"></a>Contact

Like the original template you are free to take my `.tex` file and modify it to create your own resume.  
But please don't use my resume for anything else without my permission!

If you have any questions, feel free to contact me.
