.PHONY: examples clean mrproper

CC = xelatex
EXAMPLES_DIR = examples
RESUME_DIR = examples/resume
CV_DIR = examples/cv
RESUME_SRCS = $(shell find $(RESUME_DIR) -name '*.tex')
CV_SRCS = $(shell find $(CV_DIR) -name '*.tex')

examples: $(foreach x, coverletter cv resume, $x.pdf)

fr: $(foreach x, coverletter cv resume, $x-Benjamin\ Lux-fr.pdf)

%-Benjamin\ Lux-fr.pdf: $(EXAMPLES_DIR)/%.tex $(RESUME_SRCS)
	sed -re 's/%MacroMakefileLangage/\\vf/g' $< > "$(EXAMPLES_DIR)/$(basename $@).tex"
	$(CC) -output-directory=$(EXAMPLES_DIR) "$(EXAMPLES_DIR)/$(basename $@).tex"
	rm "$(EXAMPLES_DIR)/$(basename $@).tex"


en: $(foreach x, coverletter cv resume, $x-Benjamin\ Lux-en.pdf)

%-Benjamin\ Lux-en.pdf: $(EXAMPLES_DIR)/%.tex $(RESUME_SRCS)
	sed -re 's/%MacroMakefileLangage/\\vo/g' $< > "$(EXAMPLES_DIR)/$(basename $@).tex"
	$(CC) -output-directory=$(EXAMPLES_DIR) "$(EXAMPLES_DIR)/$(basename $@).tex"
	rm "$(EXAMPLES_DIR)/$(basename $@).tex"


resume.pdf: $(EXAMPLES_DIR)/resume.tex $(RESUME_SRCS)
	$(CC) -output-directory=$(EXAMPLES_DIR) $<

cv.pdf: $(EXAMPLES_DIR)/cv.tex $(CV_SRCS)
	$(CC) -output-directory=$(EXAMPLES_DIR) $<

coverletter.pdf: $(EXAMPLES_DIR)/coverletter.tex
	$(CC) -output-directory=$(EXAMPLES_DIR) $<

clean:
	rm -rf $(EXAMPLES_DIR)/*.aux $(EXAMPLES_DIR)/*.out $(EXAMPLES_DIR)/*.log 

mrproper: clean
	rm -rf $(EXAMPLES_DIR)/*.pdf

